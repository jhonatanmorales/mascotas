import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Animal } from './clases/Animal';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mascotas';
  modal:boolean = false;
  modalEditar:boolean = false;
  imgActiva:boolean = true;
  valido:boolean = false;
  id:any = null;
  imagenIcono = "../assets/img/agregar.png";

  formulario = new FormGroup({
    id: new FormControl(""),
    nombre: new FormControl(""),
    edad: new FormControl(""),
    especie: new FormControl(""),
    img: new FormControl("")
  });

  formularioEditar = new FormGroup({
    id: new FormControl(""),
    nombre: new FormControl(""),
    edad: new FormControl(""),
    especie: new FormControl(""),
    img: new FormControl("")
  });

  listaAnimales:Animal[] = [];

  imagenesAgregar = {
    img1: "../assets/img/img1.jpeg",
    img2: "../assets/img/img2.jpg",
    img3: "../assets/img/img3.jpg",
    img4: "../assets/img/img4.jpg"
  }

  mostrarPanel(){
    this.modal = !this.modal;
    if(this.modal){
      this.imagenIcono = "../assets/img/equis.png";
    }else{
      this.imagenIcono = "../assets/img/agregar.png";
    }
  }

  mostrarPanelEditar(event:Event){
    this.modalEditar = !this.modalEditar;
    this.id = event.target as HTMLElement;

    if(this.modalEditar){
      this.imagenIcono = "../assets/img/equis.png";

      for(let i = 0; i < this.listaAnimales.length; i++){
        console.log(this.id.getAttribute("value"));
        
        if(this.id.getAttribute("value") === this.listaAnimales[i].id){
            
            this.formularioEditar.controls["id"].setValue(this.listaAnimales[i].id);
            this.formularioEditar.controls["nombre"].setValue(this.listaAnimales[i].nombre);
            this.formularioEditar.controls["edad"].setValue(this.listaAnimales[i].edad);
            this.formularioEditar.controls["especie"].setValue(this.listaAnimales[i].especie);
            this.formularioEditar.controls["img"].setValue(this.listaAnimales[i].img);
        }
      }
    }else{
      this.imagenIcono = "../assets/img/agregar.png";
    }
  }

  seleccionarImagen(event:Event){
    const elemento = event.target as HTMLElement;

    if (this.imgActiva) {
      elemento.setAttribute("class","img-active");
      this.formulario.controls["img"].setValue(elemento.getAttribute("src"));
      this.imgActiva = false;
    }else{
      elemento.removeAttribute("class");
      this.imgActiva = true;
    }
  }

  seleccionarImagenEditar(event:Event){
    const elemento = event.target as HTMLElement;

    if (this.imgActiva) {
      elemento.setAttribute("class","img-active");
      this.formularioEditar.controls["img"].setValue(elemento.getAttribute("src"));
      this.imgActiva = false;
    }else{
      elemento.removeAttribute("class");
      this.imgActiva = true;
    }
  }

  agregarAnimal(){

    const animal = new Animal();

    animal.id = this.formulario.controls["id"].value;
    animal.nombre = this.formulario.controls["nombre"].value;
    animal.edad = this.formulario.controls["edad"].value;
    animal.especie = this.formulario.controls["especie"].value;
    animal.img = this.formulario.controls["img"].value;

    this.listaAnimales.push(animal);

    this.limpiarFormulario();
  }


  mostraAnimales(){
    return this.listaAnimales;
  }

  eliminarAnimal(event:Event){
    const id = event.target as HTMLElement;
    for (let i = 0; i < this.listaAnimales.length; i++) {
      if(id.getAttribute("value") === this.listaAnimales[i].id){
        this.listaAnimales.splice(i, 1);
      }      
    }
  }

  editarAnimal(){
    for(let i = 0; i < this.listaAnimales.length; i++){
      console.log(this.id.getAttribute("value"));
      
      if(this.id.getAttribute("value") === this.listaAnimales[i].id){

          console.log("entro");

          this.listaAnimales[i].id = this.formularioEditar.controls["id"].value;
          this.listaAnimales[i].nombre = this.formularioEditar.controls["nombre"].value;
          this.listaAnimales[i].edad = this.formularioEditar.controls["edad"].value;
          this.listaAnimales[i].especie = this.formularioEditar.controls["especie"].value;
          this.listaAnimales[i].img = this.formularioEditar.controls["img"].value;

          this.listaAnimales.splice(i, 1, this.listaAnimales[i]);
      }
    }
  }

  limpiarFormulario(){
    this.formulario.controls["id"].setValue("");
    this.formulario.controls["nombre"].setValue("");
    this.formulario.controls["edad"].setValue("");
    this.formulario.controls["especie"].setValue("");
  }
  
}
